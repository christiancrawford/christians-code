import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContainerComponent } from './static/container/container.component';
import { ExperienceComponent } from './static/experience/experience.component';
import { SettingsComponent } from './settings';
import { WorkComponent } from './static/work/work.component';

const routes: Routes = [
  {
    path: 'experience',
    component: ExperienceComponent,
    data: {
      title: 'Experience'
    }
  },
  {
    path: 'container',
    component: ContainerComponent,
    data: {
      title: 'Container'
    }
  },
  {
    path: 'settings',
    component: SettingsComponent,
    data: {
      title: 'Settings'
    }
  },
  {
    path: 'work',
    component: WorkComponent,
    data: {
      title: 'Work'
    }
  }
];

@NgModule({
  // useHash supports github.io demo page, remove in your app
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
