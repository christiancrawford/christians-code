import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';

import { StaticRoutingModule } from './static-routing.module';

import { BillboardComponent } from './billboard/billboard.component';
import { ContainerComponent } from './container/container.component';
import { ExperienceComponent } from './experience/experience.component';
import { SettingsComponent } from '../settings/settings/settings.component';
import { WorkComponent } from './work/work.component';

@NgModule({
  imports: [SharedModule, StaticRoutingModule],
  declarations: [
    BillboardComponent,
    ContainerComponent,
    ExperienceComponent,
    WorkComponent
  ]
})
export class StaticModule {}
