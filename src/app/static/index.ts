export * from './static-routing.module';
export * from './static.module';

export * from './billboard/billboard.component';
export * from './container/container.component';
export * from './experience/experience.component';
export * from './work/work.component';
