# Christian's Code
by [Christian Crawford](https://www.linkedin.com/in/christiancrawford/)

Check out [Website](https://christianscode.com)

 
## Features

* lazy-loading of feature modules
* lazy reducers
* localStorage ui state persistence
* `@ngrx/effects` for API requests
* fully responsive design
* angular-material and custom components in `SharedModule`
 
## Stack

* Angular
* ngrx
* Angular Material
* Bootstrap 4 (only reset and grids)

## Build

Built with [Angular CLI](https://github.com/angular/angular-cli)
