import { ChristiansCode } from './app.po';

describe('christians-code App', () => {
  let page: ChristiansCode;

  beforeEach(() => {
    page = new ChristiansCode();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
